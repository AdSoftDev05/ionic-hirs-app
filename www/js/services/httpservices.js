//Generic service for calling API
angular.module('httpservices.module', [])

.factory('httpservices', ['$http', function ($http) {

   function JSON_to_URLEncoded(element,key,list){
          var list = list || [];
          if(typeof(element)=='object'){
            for (var idx in element)
              JSON_to_URLEncoded(element[idx],key?key+'['+idx+']':idx,list);
          } else {
            list.push(key+'='+encodeURIComponent(element));
          }
          return list.join('&');
        }
   
    return {
        getData: function (obj) {
            var xhr = $http({
                url: obj.req_url,
                method: 'GET',
                data: obj.data,
                hideLoader: obj.hideLoader,
                headers: {
                    "Content-Type": "application/json"
                }
            });
            return xhr;
        },
        setData: function (obj) {
            // console.log(obj.data);
            var xhr = $http({
                url: obj.req_url,
                method: 'POST',
                hideLoader: obj.hideLoader,
                data: obj.data,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                transformRequest: function(obj) {
                    // console.log("before transform",obj)
                    var str = [];
                    for(var p in obj){
                        if(typeof obj[p] != 'object'){
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));            
                        }else {
                            var s = [];
                            for(var d in obj[p]){
                                s.push(encodeURIComponent(d) + "=" + encodeURIComponent(obj[p][d]));
                            }
                            str.push(encodeURIComponent(p) + "=" + s);  
                        }
                    
                    }
                    
                    // console.log(str);
                    return str.join("&");
                }
            });
            return xhr;
        },

        setDataparam: function (obj) {
            // console.log(obj.data);
            var xhr = $http({
                url: obj.req_url,
                method: 'POST',
                hideLoader: obj.hideLoader,
                data: obj.data,
                headers: {
                     "Content-Type": "application/json"
                },
                  // transformRequest: function(obj) {
                  //   var  k = JSON_to_URLEncoded(obj)
                  //   console.log(k)
                  //   return k

                  // }
            });
            return xhr;
        },
        removeData: function (obj) {
            var xhr = $http({
                url: obj.req_url,
                method: 'DELETE',
                data: obj.data,
                headers: {
                    "Content-Type": "application/json"

                  
                }
            });
            return xhr;
        },
        updateData: function (obj) {
            var xhr = $http({
                url: obj.req_url,
                method: 'PUT',
                data: obj.data,
                headers: {
                    'Content-Type': 'application/json'
                   
                }
            });
            return xhr;
        },
         patchData: function (obj) {
            var xhr = $http({
                url: obj.req_url,
                method: 'PATCH',
                data: obj.data,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return xhr;
        }
    };
}]);