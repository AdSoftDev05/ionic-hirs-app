﻿//Generic service for calling API
angular.module('cordovadialogservice.module', []).factory('cordovadialogservice', ['$http','$q', '$cordovaDialogs', function ($http,$q, $cordovaDialogs) {

    return {
        alert: function (message, title, buttonname) {
            console.log('called alert');
            var deferred = $q.defer();
            $cordovaDialogs.alert(message, title, buttonname)
   .then(function (res) {
       deferred.resolve(res);
   });
            return deferred.promise;
        }
    };
}]);
