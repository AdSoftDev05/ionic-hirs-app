// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var prefix_url = "http://cruzatasoft.com/HRIS/api/";
var Image_Url = "http://cruzatasoft.com/HRIS/api/";

angular.module('starter', ['ionic','ui.rCalendar', 'starter.controllers', 'login.controllers', 'menu.controllers','employess.controllers', 'home.controllers','setting.controllers','timeoff.controllers','requesttimeoff.controllers','inbox.controllers','support.controllers','calendar.controllers', 'cordovadialogservice.module' , 'httpservices.module','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.Keyboard) {
      window.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.factory('genericInterceptor', function($q, $injector, $cordovaDialogs) {

    var interceptor = {
        'request': function(config) {
            // Successful request method
            var loadingService = $injector.get('$ionicLoading');
            if(!config.hideLoader)
            loadingService.show();
            return config; // or $q.when(config);
        },
        'response': function(response) {
            // Successful response
            if(response.data.message){
                /*$cordovaDialogs.alert(response.data.message, 'Message', 'OK')
                .then(function() {
                  // callback success
                });*/
            }
            
            var loadingService = $injector.get('$ionicLoading');
            loadingService.hide();
            return response; // or $q.when(config);
        },
        'requestError': function(rejection) {
            // An error happened on the request
            // if we can recover from the error
            // we can return a new request
            // or promise
            return response;
            // Otherwise, we can reject the next
            // by returning a rejection
            // return $q.reject(rejection);
        },
        'responseError': function(rejection) {
            
            // Returning a rejection
            /*$cordovaDialogs.alert('Please try again.', 'Something Want Worng', 'OK')
            .then(function() {
              // callback success
            });*/
            var loadingService = $injector.get('$ionicLoading');
            loadingService.hide();
            return rejection;
        }
    };
    return interceptor;
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MenuCtrl'
  })

   .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl' 
  })

   .state('profile', {
      url: '/profile',
      templateUrl: 'templates/profile.html'
      // controller: 'ProfileCtrl' 
  })

  .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'HomeCtrl'
        }
      }
  })

  .state('app.calendar', {
    url: '/calendar',
    views: {
      'menuContent': {
        templateUrl: 'templates/calendar.html',
        controller:'CalendarCtrl'
      }
    }
  })

  .state('app.request-time-off', {
    url: '/request-time-off',
    views: {
      'menuContent': {
        templateUrl: 'templates/request-time-off.html',
        controller:'RequestTimeOffCtrl'
      }
    }
  })

  .state('app.time-off', {
    url: '/time-off',
    views: {
      'menuContent': {
        templateUrl: 'templates/time-off.html',
        controller:'TimeOffCtrl'
      }
    }
  })

  .state('app.inbox', {
      url: '/inbox',
      views: {
        'menuContent': {
          templateUrl: 'templates/inbox.html',
          controller: 'InboxCtrl'
        }
      }
  })

  .state('app.setting', {
    url: '/setting',
    views: {
      'menuContent': {
        templateUrl: 'templates/setting.html',
        controller:'SettingCtrl'
      }
    }
  })

   .state('app.support', {
    url: '/support',
    views: {
      'menuContent': {
        templateUrl: 'templates/support.html',
        controller:'SupportCtrl'
      }
    }
  })

  .state('app.employess', {
    url: '/employess',
    views: {
      'menuContent': {
        templateUrl: 'templates/employess.html',
        controller:'EmployeesCtrl'
      }
    }
  })

  // .state('app.myTemplate', {
  //   url: '/myTemplate',
  //   views: {
  //     'menuContent': {
  //       templateUrl: 'templates/myTemplate.html',
  //       controller:'calendarCtrl'
  //     }
  //   }
  // });
  // if none of the above states are matched, use this as the fallback
  if(localStorage.getItem('userDetails')){
           $urlRouterProvider.otherwise('/app/home');  
    }else{
           $urlRouterProvider.otherwise('login');
    };
});
