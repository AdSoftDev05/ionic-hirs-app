angular.module('employess.controllers', [])

.controller('EmployeesCtrl', function($scope, $ionicPopover, $state, httpservices, $state, $ionicModal, $ionicLoading,cordovadialogservice) {

		 if (localStorage.getItem("userDetails")) {
		        $scope.usedetails = JSON.parse(localStorage.getItem("userDetails"));
		        console.log($scope.usedetails)
		        $scope.eId = $scope.usedetails.e_id
		        console.log("=======E_id===", $scope.eId)
		    }

			     $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'employee/' + $scope.eId
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.Employees = resp.data.data;
                console.log(" == Employee == ", $scope.Employees)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

            $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'country_list'
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();

                $scope.CountryList = resp.data.data;
                console.log(" == CountryList == ", $scope.CountryList)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

             $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'province_list'
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.ProvinceList = resp.data.data;
                console.log(" == province_list == ", $scope.ProvinceList)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });



            $scope.goDetails = function(details){

                console.log(details)
            }

             $ionicPopover.fromTemplateUrl('templates/modal/popover.html', {
                  scope: $scope
               }).then(function(popover) {
                  $scope.popover = popover;
               });

               $scope.openPopover = function($event) {
                   
                  $scope.popover.show($event);
               };

               $scope.closePopover = function() {
                  $scope.popover.hide();
               };

               $ionicModal.fromTemplateUrl('templates/modal/profile.html', {
                scope: $scope
              }).then(function(modal) {
                $scope.modal = modal;
              });

              // Triggered in the login modal to close it
              $scope.CloseProfilePopup = function() {
                $scope.modal.hide();
              };

              // Open the login modal
              $scope.ProfileData ={}
              $scope.OpenProfilePopup = function(lis) {
                console.log(lis)
                $ionicLoading.show();
                var data = {
                    req_url: prefix_url + 'employee_info/' + lis.e_id
                }
                httpservices.getData(data).then(function(resp) {
                    $ionicLoading.hide();
                    $scope.id = $scope.eId;
                    $scope.emp = resp.data.data;
                    console.log(" == Emp == ", $scope.emp)  
                    $scope.ProfileData = lis
                    $scope.update = $scope.emp["personal"];
                    console.log($scope.update)
                    $scope.modal.show();              
                }, function(err) {
                    $ionicLoading.hide();
                    cordovadialogservice.alert(err, 'Alert', 'Ok')
                    console.log(err)
                });

               
              }

                $scope.updateDate = {}
                $scope.UpdateProfile = function (update) {
                  console.log(update)
                  $scope.updateDate = update
                  console.log($scope.updateDate)
                    // $scope.update.e_id = $scope.eId
                    // var checkedOnce = true;
                    // var errorPara = '';
                    // if (!update.home_phone) {
                    //     cordovadialogservice.alert('Number field is required', 'Alert', 'Ok').then(function (res) {
                    //         return;
                    //     });
                    // } 
                    // else {
                        $ionicLoading.show();
                        var data = { req_url: prefix_url + "employee_info/" + update.e_id, data:update}
                        httpservices.setData(data).then(function (response) {
                            $ionicLoading.hide();
                            $scope.ProfileUpdateData = response.data
                            console.log($scope.ProfileUpdateData)
                            cordovadialogservice.alert($scope.ProfileUpdateData.message)
                        }, function (error) {
                            $ionicLoading.hide();
                            $scope.update = {}
                            alert("Invalid Data")
                            console.log(error)
                        })
                    // }
              }
              
})