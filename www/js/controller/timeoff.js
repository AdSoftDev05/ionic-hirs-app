angular.module('timeoff.controllers', [])

.controller('TimeOffCtrl',  function($scope,$state,httpservices,$ionicModal,$state, $ionicLoading,cordovadialogservice) {

		 if (localStorage.getItem("userDetails")) {
              $scope.usedetails = JSON.parse(localStorage.getItem("userDetails"));
              console.log($scope.usedetails)
              $scope.eId = $scope.usedetails.e_id
              console.log("=======E_id===", $scope.eId)
          }

         $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'whos_out/' + $scope.eId
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.WhosData = resp.data.data;
                console.log(" == whos out Data == ", $scope.WhosData)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

            var data = {
                req_url: prefix_url + 'time_off/' + $scope.eId
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.timeoff = resp.data.data;
                console.log(" == time_off == ", $scope.timeoff)
                $scope.Sliders = $scope.timeoff.slider
                $scope.HistoryEvent = $scope.timeoff.history
                $scope.UpcomingEvent = $scope.timeoff.upcoming
                console.log($scope.Sliders)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

            $ionicModal.fromTemplateUrl('templates/modal/profile.html', {
                scope: $scope
              }).then(function(modal) {
                $scope.modal = modal;
              });

              // Triggered in the login modal to close it
              $scope.CloseProfilePopup = function() {
                $scope.modal.hide();
              };

              // Open the login modal
              $scope.OpenProfilePopup = function() {
                $scope.modal.show();
              }

              $ionicModal.fromTemplateUrl('templates/modal/request-details.html', {
                scope: $scope
              }).then(function(modal1) {
                $scope.modal1 = modal1;
              });
              // Triggered in the login modal to close it
              $scope.CloseRequestPopup = function() {
                $scope.modal1.hide();
              };

              // Open the login modal
              $scope.OpenRequestPopup = function(his) {
                console.log(his)
                $scope.HitstorData = his
                $scope.modal1.show();
              }

              $ionicModal.fromTemplateUrl('templates/modal/new-time-request.html', {
                scope: $scope
              }).then(function(modal2) {
                $scope.modal2 = modal2;
              });
              // Triggered in the login modal to close it
              $scope.CloseNewRequestPopup = function() {
                $scope.modal2.hide();
              };

              // Open the login modal
              $scope.OpenNewRequestPopup = function() {
                console.log()
                $scope.modal2.show();
              }

                $scope.update = {home_phone : ""}
                $scope.UpdateProfile = function (update) {
                    $scope.update.e_id = $scope.eId
                    var checkedOnce = true;
                    var errorPara = '';
                    if (!update.home_phone) {
                        cordovadialogservice.alert('Number field is required', 'Alert', 'Ok').then(function (res) {
                            return;
                        });
                    } 
                    else {
                        $ionicLoading.show();
                        var data = { req_url: prefix_url + "employee_info", data:update}
                        httpservices.setData(data).then(function (response) {
                            $ionicLoading.hide();
                            $scope.ProfileUpdateData = response.data
                            console.log($scope.ProfileUpdateData)
                            cordovadialogservice.alert($scope.ProfileUpdateData.message)
                        }, function (error) {
                            $ionicLoading.hide();
                            $scope.update = {}
                            alert("Invalid Data")
                            console.log(error)
                        })
                    }
              }

              $scope.request = {}
              $scope.requestDate = {}
              $scope.SendRequest = function(request){
                console.log(request)
                $scope.requestDate = request

                $scope.ToDate = request.to_date
                $scope.FromDate = request.from_date
                var dd = $scope.FromDate.getDate();
                var mm = $scope.FromDate.getMonth()+1;
                var yyyy = $scope.FromDate.getFullYear();
                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                }
                $scope.Filterfromto = yyyy + '-' + mm + '-' + dd;
                console.log($scope.Filterfromto)
                var dd = $scope.ToDate.getDate();
                var mm = $scope.ToDate.getMonth()+1;
                var yyyy = $scope.ToDate.getFullYear();
                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                }
                $scope.Filtertodate = yyyy + '-' + mm + '-' + dd;
                console.log($scope.Filtertodate)

                $scope.requestDate.from_date = $scope.Filterfromto
                $scope.requestDate.to_date = $scope.Filtertodate
                $scope.requestDate.type = "Day"
                $scope.requestDate.e_id = $scope.eId

                    $ionicLoading.show();
                        var data = { req_url: prefix_url + "request_time_off", data:$scope.requestDate}
                        httpservices.setData(data).then(function (response) {
                            $ionicLoading.hide();
                            $scope.RequestData = response.data
                            if($scope.RequestData.error == false){
                              $scope.modal1.hide();
                            }
                            cordovadialogservice.alert($scope.RequestData.message)
                            // console.log($scope.RequestData)
                            
                        }, function (error) {
                            $ionicLoading.hide();
                            $scope.update = {}
                            alert("Invalid Data")
                            console.log(error)
                        })
              }
              


            $scope.goRequestPage = function(){
               // $state.go("app.request-time-off")
            }

            $scope.goCalendarPage = function(){
               // $state.go("app.calendar")
            }

})