angular.module('setting.controllers', [])

.controller('SettingCtrl', function ($scope, $rootScope, $ionicLoading, $state,$ionicPopup) {

    $scope.logOut = function(){
       var confirmPopup = $ionicPopup.confirm({
         title: 'Logout',
         template: 'Are you sure, you want to Logout?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           localStorage.clear();
           $state.go("login");
         } else {
         }
       });
    }

})