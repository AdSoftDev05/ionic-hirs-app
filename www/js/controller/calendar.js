angular.module('calendar.controllers', [])

.controller('CalendarCtrl', function($scope,$state,httpservices,$state,$ionicPopover, $ionicLoading,$ionicModal,cordovadialogservice) {

        $scope.selectedDate = new Date();
        $scope.eventSource = [];

         $ionicPopover.fromTemplateUrl('templates/modal/popover.html', {
                  scope: $scope
               }).then(function(popover) {
                  $scope.popover = popover;
               });

               $scope.openPopover = function($event) {
                  $scope.popover.show($event);
               };

               $scope.closePopover = function() {
                  $scope.popover.hide();
               };

        $ionicModal.fromTemplateUrl('templates/modal/profile.html', {
        scope: $scope
        }).then(function(modal) {
        $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.CloseProfilePopup = function() {
        $scope.modal.hide();
        };

		 if (localStorage.getItem("userDetails")) {
		        $scope.usedetails = JSON.parse(localStorage.getItem("userDetails"));
		        console.log($scope.usedetails)
		        $scope.eId = $scope.usedetails.e_id
		        console.log("=======E_id===", $scope.eId)
		    }

               var filtersdate = new Date()
               var dd = filtersdate.getDate();
               var mm = filtersdate.getMonth()+1;
                var yyyy = filtersdate.getFullYear();
                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                }
                filtersdate = yyyy + '-' + mm + '-' + dd;
                console.log(filtersdate)
                $scope.DateSend = filtersdate
			     $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'calendar/?date=' + $scope.DateSend
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                console.log(resp.data.data)
                $scope.CalenDar = resp.data.data.month;
                var event = resp.data.data.onday;
                 evntsrc = resp.data.data.month;
                if(event){
                    event.forEach(element => {
                        // element.allDay = true;
                        evntsrc.push(element);
                    });
                }
                console.log(event);
                if(evntsrc){
                    evntsrc.forEach(element => {
                        console.log(element.from_date)
                        var yyyy_from_date = element.from_date.split("-");
                        var yyyy_from = parseInt(yyyy_from_date[0]);
                        var mm_from = parseInt(yyyy_from_date[1]);
                        var dd_from = parseInt(yyyy_from_date[2]);
                        console.log(yyyy_from);

                        console.log(element.to_date)
                        var yyyy_to_date = element.to_date.split("-");
                        var yyyy_to = parseInt(yyyy_to_date[0]);
                        var mm_to = parseInt(yyyy_to_date[1]);
                        var dd_to = parseInt(yyyy_to_date[2]);
                        console.log(yyyy_to_date);
//                         const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
//   "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
// ];                            
//                         console.log(monthNames[parseInt(mm_from)-1]);
                        
                        var newObj = {
                            "title":element.title,
                            "startTime": new Date(Date.UTC(yyyy_from, mm_from , dd_from)),
                            "endTime": new Date(Date.UTC(yyyy_to, mm_to, dd_to)),
                            "allDay":true,
                            "name":element.name,
                            "img":"img/Gentleman.jpg",
                            "e_id":element.e_id
                            // "month":monthNames[parseInt(mm_from)-1]
                        }
                        $scope.eventSource.push(newObj);
                    });
                }
               
                console.log($scope.eventSource);
                console.log(" == Calendar Data == ", $scope.CalenDar)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });
            

            $scope.onEventSelected = function (event) {
                //  alert(event.e_id);
                    $ionicLoading.show();
                    var data = {
                        req_url: prefix_url + 'employee_info/' + event.e_id
                    }
                    httpservices.getData(data).then(function(resp) {
                        $ionicLoading.hide();
                        $scope.emp = resp.data.data;
                        console.log(" == Emp == ", $scope.emp)  
                        $scope.ProfileData = event
                        $scope.update = $scope.emp["personal"];
                        console.log($scope.update)
                        $scope.modal.show();              
                    }, function(err) {
                        $ionicLoading.hide();
                        cordovadialogservice.alert(err, 'Alert', 'Ok')
                        console.log(err)
                    });
            };
            
            $scope.monthviewEventDetailTemplateUrl = './templates/mytemplate.html';
})