angular.module('inbox.controllers', [])

.controller('InboxCtrl', function ($scope,httpservices, $rootScope, $ionicLoading, $state,$ionicPopup,$ionicPopover) {

     if (localStorage.getItem("userDetails")) {
            $scope.usedetails = JSON.parse(localStorage.getItem("userDetails"));
            console.log($scope.usedetails)
            $scope.eId = $scope.usedetails.e_id
            console.log("=======E_id===", $scope.e_id)
        }

      $ionicLoading.show();
      var data = {
          req_url: prefix_url + 'request_inbox'
      }
      httpservices.getData(data).then(function(resp) {
          $ionicLoading.hide();

          $scope.inboxData = resp.data.data;
          console.log(" == inboxData == ", $scope.inboxData)                
      }, function(err) {
          $ionicLoading.hide();
          cordovadialogservice.alert(err, 'Alert', 'Ok')
          console.log(err)
      });


    $scope.logOut = function(){
       var confirmPopup = $ionicPopup.confirm({
         title: 'Logout',
         template: 'Are you sure, you want to Logout?'
       });
       confirmPopup.then(function(res) {
         if(res) {
           localStorage.clear();
           $state.go("login");
         } else {
         }
       });
    }

    $ionicPopover.fromTemplateUrl('templates/modal/inboxpop.html', {
                  scope: $scope
               }).then(function(popover) {
                  $scope.popover = popover;
               });

               $scope.openPopover = function($event) {
                  $scope.popover.show($event);
               };

               $scope.closePopover = function() {
                  $scope.popover.hide();
               };

})