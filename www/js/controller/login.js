angular.module('login.controllers', [])

.controller('LoginCtrl', function ($scope,$state,httpservices,$state, $ionicLoading,cordovadialogservice) {

     $scope.user = {email : "" , password : ""}
     $scope.SendSocialData = {}
     $scope.loginData = {}

    $scope.LoggedinUser = function (user) {
        var checkedOnce = true;
        var errorPara = '';
        if (!user.email) {
            cordovadialogservice.alert('Email field is required', 'Alert', 'Ok').then(function (res) {
                return;
            });
        }  else if (!user.password) {
            cordovadialogservice.alert('Password field is required', 'Alert', 'Ok').then(function (res) {
                return;
            });
        } 

        else {
            
            $ionicLoading.show();
            var data = { req_url: prefix_url + "login", data:user}
            httpservices.setData(data).then(function (response) {
                $ionicLoading.hide();
                $scope.loginData = response.data
                console.log($scope.loginData)
                cordovadialogservice.alert($scope.loginData.message)
                localStorage.setItem("userDetails",JSON.stringify($scope.loginData.data));
                $state.go("app.home")
            }, function (error) {
                $ionicLoading.hide();
                $scope.user = {}
                alert("Invalid User Id Password")
                console.log(error)
            })
        }
  }
      
})
