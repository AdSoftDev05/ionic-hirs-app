angular.module('home.controllers', [])

.controller('HomeCtrl', function($scope,$state,httpservices,$ionicModal,$state, $ionicLoading,cordovadialogservice) {

		 if (localStorage.getItem("userDetails")) {
		        $scope.usedetails = JSON.parse(localStorage.getItem("userDetails"));
		        console.log($scope.usedetails)
		        $scope.eId = $scope.usedetails.e_id
		        console.log("=======E_id===", $scope.eId)
            }
            
            $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'request_inbox' 
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.inboxReqData = resp.data.data;
                console.log(" == inbox request Data == ", $scope.inboxReqData)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

			$ionicLoading.show();
            var data = {
                req_url: prefix_url + 'whos_out/' + $scope.eId
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.WhosData = resp.data.data;
                console.log(" == whos out Data == ", $scope.WhosData)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

            var data = {
                req_url: prefix_url + 'time_off/' + $scope.eId
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.timeoff = resp.data.data;
                console.log(" == time_off == ", $scope.timeoff)
                $scope.Sliders = $scope.timeoff.slider
                $scope.HistoryEvent = $scope.timeoff.history
                console.log($scope.Sliders)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

            $ionicModal.fromTemplateUrl('templates/modal/profile.html', {
                scope: $scope
              }).then(function(modal) {
                $scope.modal = modal;
              });

              // Triggered in the login modal to close it
              $scope.CloseProfilePopup = function() {
                $scope.modal.hide();
              };

              // Open the login modal
              $scope.OpenProfilePopup = function(lis) {
                console.log(lis)
                $ionicLoading.show();
               
                var data = {
                    req_url: prefix_url + 'employee_info/' + lis.t_id
                }
                httpservices.getData(data).then(function(resp) {
                    $ionicLoading.hide();
                    $scope.id = $scope.eId;
                    $scope.emp = resp.data.data;
                    console.log(" == Emp == ", $scope.emp)  
                    $scope.ProfileData = lis
                    $scope.update = $scope.emp["personal"];
                    console.log($scope.update)
                    $scope.modal.show();              
                }, function(err) {
                    $ionicLoading.hide();
                    cordovadialogservice.alert(err, 'Alert', 'Ok')
                    console.log(err)
                });
                // $scope.modal.show();
              }

                $scope.update = {home_phone : ""}
                $scope.UpdateProfile = function (update) {
                    $scope.update.e_id = $scope.eId
                    var checkedOnce = true;
                    var errorPara = '';
                    if (!update.home_phone) {
                        cordovadialogservice.alert('Number field is required', 'Alert', 'Ok').then(function (res) {
                            return;
                        });
                    } 
                    else {
                        $ionicLoading.show();
                        var data = { req_url: prefix_url + "employee_info", data:update}
                        httpservices.setData(data).then(function (response) {
                            $ionicLoading.hide();
                            $scope.ProfileUpdateData = response.data
                            console.log($scope.ProfileUpdateData)
                            cordovadialogservice.alert($scope.ProfileUpdateData.message)
                        }, function (error) {
                            $ionicLoading.hide();
                            $scope.update = {}
                            alert("Invalid Data")
                            console.log(error)
                        })
                    }
              }


           
                  $ionicModal.fromTemplateUrl('templates/modal/new-time-request.html', {
                  scope: $scope
                  }).then(function(modal1) {
                    $scope.modal1 = modal1;
                  });
                  // Triggered in the login modal to close it
                  $scope.CloseNewRequestPopup = function() {
                    $scope.modal1.hide();
                  };

                  // Open the login modal
                  $scope.OpenNewRequestPopup = function() {
                    console.log()
                    $scope.modal1.show();
                  }

            $scope.request = {}
              $scope.requestDate = {}
              $scope.SendRequest = function(request){
                console.log(request)
                $scope.requestDate = request

                $scope.ToDate = request.to_date
                $scope.FromDate = request.from_date
                var dd = $scope.FromDate.getDate();
                var mm = $scope.FromDate.getMonth()+1;
                var yyyy = $scope.FromDate.getFullYear();
                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                }
                $scope.Filterfromto = yyyy + '-' + mm + '-' + dd;
                console.log($scope.Filterfromto)
                var dd = $scope.ToDate.getDate();
                var mm = $scope.ToDate.getMonth()+1;
                var yyyy = $scope.ToDate.getFullYear();
                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                }
                $scope.Filtertodate = yyyy + '-' + mm + '-' + dd;
                console.log($scope.Filtertodate)

                $scope.requestDate.from_date = $scope.Filterfromto
                $scope.requestDate.to_date = $scope.Filtertodate
                $scope.requestDate.type = "Day"
                $scope.requestDate.e_id = $scope.eId

                    $ionicLoading.show();
                        var data = { req_url: prefix_url + "request_time_off", data:$scope.requestDate}
                        httpservices.setData(data).then(function (response) {
                            $ionicLoading.hide();
                            $scope.RequestData = response.data
                            if($scope.RequestData.error == false){
                              $scope.modal1.hide();
                            }
                            cordovadialogservice.alert($scope.RequestData.message)
                            // console.log($scope.RequestData)
                            
                        }, function (error) {
                            $ionicLoading.hide();
                            $scope.update = {}
                            alert("Invalid Data")
                            console.log(error)
                        })
              }

            $scope.goCalendarPage = function(){
            	$state.go("app.calendar")
            }

            $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'country_list'
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();

                $scope.CountryList = resp.data.data;
                console.log(" == CountryList == ", $scope.CountryList)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });

             $ionicLoading.show();
            var data = {
                req_url: prefix_url + 'province_list'
            }
            httpservices.getData(data).then(function(resp) {
                $ionicLoading.hide();
                $scope.ProvinceList = resp.data.data;
                console.log(" == province_list == ", $scope.ProvinceList)                
            }, function(err) {
                $ionicLoading.hide();
                cordovadialogservice.alert(err, 'Alert', 'Ok')
                console.log(err)
            });
            $scope.openShare = function (){
                var options = {
                    message: 'share this', // not supported on some apps (Facebook, Instagram)
                    subject: 'the subject', // fi. for email
                    files: ['', ''], // an array of filenames either locally or remotely
                    url: 'https://www.website.com/foo/#bar?a=b',
                    chooserTitle: 'Pick an app', // Android only, you can override the default share sheet title,
                    // appPackageName: 'com.apple.social.facebook' // Android only, you can provide id of the App you want to share with
                  };
                  
                  var onSuccess = function(result) {
                    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                    console.log("Shared to app: " + result.app); // On Android result.app since plugin version 5.4.0 this is no longer empty. On iOS it's empty when sharing is cancelled (result.completed=false)
                  };
                  
                  var onError = function(msg) {
                    console.log("Sharing failed with message: " + msg);
                  };
                  
                  window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
            }

})